'''
Created on Aug 16, 2013

@author: Andreas
'''

import base_command
from datetime import datetime
from dateutil import tz
import re
from threading import Timer
import time
import os

from wand.font import Font
from wand.image import Image

from random import choice
from random import shuffle

class CommandScrambler(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"scrambler": "Format: scrambler 'command' Possible commands: start, stop, leader"}
        self.bot = bot
        self.running = False
        self.jid_to = None
        
        self.words = {}
        self.winners = []
        
        
        self.loadWords("Animals")
        self.loadWords("Computers")
        self.loadWords("Atmosphere")
        self.loadWords("Football Teams")
        self.loadWords("Hockey Teams")
        self.loadWords("Periodic Table")
        self.loadWords("Rocks and Minerals")
        self.loadWords("Solar System")
        self.loadWords("States")
        self.loadWords("Top 100 Movies of All Time")
        self.loadWords("Weather")
        
        self.getWordImageURL("hello world")
            
    def loadWords(self, category):
        self.words[category] = []
        f = open('../scrambler_word_list/'+category.lower().replace(' ', '_')+'.txt', 'r')
        for line in f:
            self.words[category].append(line.strip())
        
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        if arg=="start":
            self.jid_to = jid_to
            self.start()
        elif arg=="stop":
            self.stop()
            
    def start(self):
        self.current_word = ''
        self.current_category = ''
        self.waiting_for_answer = False
        self.current_word_count = 0;
        if not self.running:
            self.outputHeader(["New game beginning now!"])
            self.bot.add_event_handler("groupchat_message", self.receiveMessage)
            self.running = True
            self.queueNewWord()
    
    def stop(self):
        self.bot.del_event_handler("groupchat_message", self.receiveMessage)
        self.running = False
        Timer.cancel()
        self.waiting_for_answer = False
        self.current_word = None
        
    def receiveMessage(self, msg):
        if msg['mucnick'] != self.bot.nick:
            if self.waiting_for_answer and msg['body']==self.current_word:
                self.correctAnswer(msg['mucnick'])
                
    def queueNewWord(self):
        print("queueing new word")
        
        if self.word_image_path != None:
            os.remove(self.word_image_path)
        
        if self.current_word_count==10:
            self.endRound()
            
        self.waiting_for_answer = False
        seconds = self.bot.config.get("scrambler", "seconds_between_words")
        Timer(5, self.sendMessage, [["New word in "+seconds+" seconds"]]).start()
        Timer(int(seconds)+5, self.newWord).start()
        
    def newWord(self):
        if self.running:
            self.current_word_count+=1
            word = self.pickRandomWord()
            print("Word: "+self.current_word)
            word = self.scrambleWord(word)
            img_tag = self.getWordImageURL(word)
            self.outputHeader(["Category: "+self.current_category,"    Word: "+img_tag])
            self.waitForAnswer(20)
            
    def getWordImageURL(self, word):
        width = len(word) * 8
        height = 15
        
        font = Font(path='../scrambler_images/courier.ttf', size=13)
        with Image(width=width, height=height) as image:
            image.caption(word, left=0, top=0, font=font)
            self.word_image_path = '../scrambler_images/'+word+'.png'
            image.save(filename=self.word_image_path)

        return '<img src="'+self.bot.config.get('scrambler', 'image_url')+'/scrambler_images/'+word+'.png">'
            
    def timesUp(self):
        if self.running:
            if self.waiting_for_answer and self.current_word != '':
                self.sendMessage(["Time's up!","Word was: "+self.current_word])
                self.queueNewWord()
        
        
    def waitForAnswer(self, delay):
        if self.running:
            self.waiting_for_answer = True
            Timer(delay, self.timesUp, ()).start()
        
    def pickRandomWord(self):
        self.current_category = choice(self.words.keys())
        self.current_word = choice(self.words[self.current_category])
        return self.current_word
    
    def scrambleWord(self, str):
        words = str.split()
        shuffled_words = []
        for word in words:
            shuffled_word = list(word)
            shuffle(shuffled_word)
            shuffled_words.append(''.join(shuffled_word))
        return ' '.join(shuffled_words)
    
    def correctAnswer(self, nick):
        print "correct answer"
        if(self.running):
            self.current_word = ''
            self.current_category = ''
            nick = ''.join(nick.split(' '))
            self.sendMessage(["@"+nick+" is correct!"], 'green')
            exists = False
            for i, v in enumerate(self.winners):
                if v[1]==nick:
                    exists = True
                    v[0]+=1
                
            if not exists:
                self.winners.append([1, nick]);
                    
            self.winners.sort(reverse=True)
            
            self.queueNewWord()
    
    def outputHeader(self, message=[]):
        if len(self.winners)==0:
            current_leader = ["No current leader"]
        else:
            current_leader = ["Current leader: @"+self.winners[0][1],
                              "         Score: "+str(self.winners[0][0])]
            
        self.sendMessage([" ",
                          "--------------------",
                          "Scrambot 9000"]+
                          current_leader+
                          message+
                          ["--------------------",
                           " "])
        
    def endRound(self):
        mins = self.bot.config.get("scrambler", "minutes_between_rounds")
        self.outputHeader(["End of Round", "Next round in "+mins+" mins"])
        self.stop()
        Timer(int(mins), self.start)
        
    def sendMessage(self, message, color=''):
        msg = ""
        for i in message:
            if(i==" "):
                prefix = ""
            else:
                prefix = "-][- "
            msg += prefix+i+"<br>"
        self.bot.sendMessage(self.jid_to, '<pre>'+msg+'</pre>', 'html', '', color)