'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_command
import re
import urllib2
from lxml import etree, html

class CommandDefine(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"define": "Format: define x - defines x"}
        self.bot = bot
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        definition = self.lookup(arg)
        self.bot.sendMessage(jid_to, definition, 'html', mention_name)
        
    #Taken from SkyBot: https://github.com/rmmh/skybot/blob/master/plugins/dictionary.py
    def lookup(self, word):
        url = 'http://ninjawords.com/'

        response = urllib2.urlopen(url + word)
        h = html.fromstring(response.read())
    
        definition = h.xpath('//dd[@class="article"] | '
                             '//div[@class="definition"] |'
                             '//div[@class="example"]')
    
        if not definition:
            return 'No results for ' + word
        
        def format_output(show_examples):
            result = '<b>%s</b> ' % h.xpath('//dt[@class="title-word"]/a/text()')[0]
    
            correction = h.xpath('//span[@class="correct-word"]/text()')
            if correction:
                result = 'definition for <b><i>"%s"</i></b>: ' % correction[0]
    
            sections = []
            for section in definition:
                if section.attrib['class'] == 'article':
                    sections += [['<br><i>'+section.text_content() + ': </i>']]
                elif section.attrib['class'] == 'example':
                    if show_examples:
                        sections[-1][-1] += ' ' + section.text_content()
                else:
                    sections[-1] += [section.text_content()]
    
            for article in sections:
                result += article[0]
                if len(article) > 2:
                    result += ' '.join('%s. %s' % ('<br><b>'+ str(n + 1) + "</b>", section)
                                       for n, section in enumerate(article[1:]))
                else:
                    result += article[1] + ' '
    
            synonyms = h.xpath('//dd[@class="synonyms"]')
            if synonyms:
                result += "<br><b>"+synonyms[0].text_content()+"</b>"
    
            result = re.sub(r'\s+', ' ', result)
            result = re.sub('\xb0', '', result)
            return result
    
        result = format_output(True)
        if len(result) > 450:
            result = format_output(False)
    
        if len(result) > 450:
            result = result[:result.rfind(' ', 0, 450)]
            result = re.sub(r'[^A-Za-z]+\.?$', '', result) + ' ...'
        
        return result