'''
Created on Aug 15, 2013

@author: Andreas
'''

class BaseListener(object):


    def __init__(self, bot):
        self.description = ""
        self.bot = bot
    
    def getDescription(self):
        return self.description
    
    def receiveMessage(self, jid_to, from_tuple, message):
        pass