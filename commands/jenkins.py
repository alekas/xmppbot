'''
Created on Aug 16, 2013

@author: Andreas
'''

import base_command
from jenkinsapi.jenkins import Jenkins
from datetime import datetime
from dateutil import tz
import re

class CommandJenkins(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"jenkins": "Format: jenkins 'command' 'project'[ build number]. Possible commands: status, build [no build number supported], console"}
        self.bot = bot
        self.J = Jenkins(self.bot.config.get('jenkins', 'url'), self.bot.config.get('jenkins', 'username'), self.bot.config.get('jenkins', 'password'))
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        m= re.search("^(.*?) (.*?)( (\d+))?$", arg)
        if m:
            command = m.groups()[0].strip()
            job_name = m.groups()[1].strip()
            
            build_number = None
            if m.groups()[2] != None:
                build_number = m.groups()[2].strip()
        else:
            self.bot.sendMessage(jid_to, "Jenkins argument not in the correct format. Try \"subcommand jobname[ build number]\"", 'html', mention_name)
            return
        
        if not self.jobExists(job_name):
            self.bot.sendMessage(jid_to, "Job "+job_name+" was not found. Case matters!", 'html', mention_name)
            return
        job = self.J[job_name]
        if build_number!=None:
            build = job.get_build(int(build_number))
        else:
            build = job.get_last_build()
            
        if command=="status":
            build_time = build.get_timestamp()
            from_zone = tz.tzutc();
            to_zone = tz.tzlocal();
            
            build_time = build_time.replace(tzinfo=from_zone)
            build_time = build_time.astimezone(to_zone)
            print(build.get_status())
            print(build_time.strftime("%I:%M %p on %a %b %d, %Y"))
            self.bot.sendMessage(jid_to, str(job_name)+" build #"+str(build.get_number())+" was built at "+build_time.strftime("%I:%M %p on %a %b %d, %Y")+" and was a "+build.get_status(), 'html', mention_name)
            #self.bot.send_message(jid_to, " build test")
        elif command=="build":
            if job.is_queued_or_running():
                self.bot.sendMessage(jid_to, "Job "+job_name+" is currently queued to build or is already running", 'html', mention_name)
            else:
                next_build = job.get_next_build_number()
                job.invoke()
                self.bot.sendMessage(jid_to, "Build #"+str(next_build)+ " queued.", 'html', mention_name)
        elif command=="console":
            #if not isinstance(connection, irc.DCCConnection):
            #    connection.privmsg(nick, "This command requires a DCC connection. Please use \".dcc\" to request one.")
            #else:
                lines = "<br>".join(build.get_console().split("\n"))
                self.bot.sendMessage(jid_to, lines, 'html', mention_name)
    
    def jobExists(self, job_name):
        return job_name in self.J
            