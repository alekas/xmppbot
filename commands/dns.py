'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_command
import random
from subprocess import call

class CommandDNS(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"dns": "Format: dns 'command' Possible Commands: clearcache"}
        self.bot = bot
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        if arg=="clearcache":
            return_code = call(["c:\windows\sysnative\dnscmd.exe", "/ClearCache"])
            if return_code == 0:
                self.bot.sendMessage(jid_to, 'DNS Cache Cleared.', 'html', mention_name)
            else:
                self.bot.sendMessage(jid_to, 'An error occurred while trying to clear DNS cache', 'html', mention_name)
        else:
            self.bot.sendMessage(jid_to, 'An error occurred: <b>'+arg+'</b> is not a reconized argument', 'html', mention_name)