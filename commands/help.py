'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_command

class CommandHelp(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"help": "Outputs this help message", "reload": "Reloads plugins"}
        self.bot = bot
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        if command=='reload':
            self.bot.importCommands();
        else:
            commands = self.bot.getCommands()
            msg = "<b>Here is a list of available commands</b><br><ul>"
            for k in commands:
                msg += "<li><b>"+self.bot.getCommandPrefix()+k+"</b> - "+commands[k][1]+"<br></li>";
                
            msg+="</ul><b>Here is a list of current listeners</b><br><ul>"
            for l in self.bot.getListeners():
                msg+="<li>"+l[1]+"</li>"
            msg+="</ul>"
            self.bot.sendMessage(jid_to, msg, 'html', mention_name)