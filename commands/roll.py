'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_command
import random

class CommandRoll(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"roll": "Format: roll[ n] - Rolls a Dn"}
        self.bot = bot
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        self.bot.sendMessage(jid_to, from_tuple[2]+' rolls a %s' % random.randint(1,int(arg)), 'html', mention_name)