'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_listener
import re

class ListenerProtolol(base_listener.BaseListener):

    def __init__(self, bot):
        self.description = "Protolol"
        self.bot = bot
    
    def receiveMessage(self, jid_to, from_tuple, message):
        if message[:8]=="protolol":
            matches = re.match("^protolol(\s*>\s*(@[^ ]+?))\s?$", message)
            print message
            to = from_tuple[2]
            print matches
            if matches:
                print matches.groups()
                if matches.groups()[1] != None:
                    to = matches.groups()[1]
                    
            self.bot.sendMessage(jid_to, to.strip()+" forgot to say <em>lol</em>")