'''
Created on Feb 19, 2014

@author: Andreas
'''
import base_command

class StateMachineCommand(base_command.BaseCommand):
    def transition(self, state):
        raise NotImplementedError()