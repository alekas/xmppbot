'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_listener
import re
import urllib2
from lxml import etree, html

class ListenerScreencast(base_listener.BaseListener):

    def __init__(self, bot):
        self.description = "Screencast Image Scraper"
        self.bot = bot
    
    def receiveMessage(self, jid_to, from_tuple, message):
        matches = re.match(".*?(http:\/\/screencast.com\/t\/[a-zA-Z0-9]*)($|\s)", message)
        if matches:
            if matches.groups()[0] != None:
                response = urllib2.urlopen(matches.groups()[0])
                response_text = response.read()
                h = html.fromstring(response_text)
                
                html_part = h.xpath('//div[@id="mediaDisplayArea"]/a/@href')
                a_href = "".join(html_part)
                if a_href=='': 
                    self.bot.sendMessage(jid_to, from_tuple[1]+" shared a Screencast. It is not a format I can link to directly, though.", 'text')
                else:                
                    self.bot.sendMessage(jid_to, from_tuple[1]+" shared a Screencast. Direct link: "+a_href, 'text')