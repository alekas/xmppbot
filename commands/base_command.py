'''
Created on Aug 15, 2013

@author: Andreas
'''

class BaseCommand(object):


    def __init__(self, bot):
        self.commands = []
        self.bot = bot
    
    def getCommands(self):
        return self.commands
    
    def execute(self, jid_to, from_tuple, command, arg):
        pass