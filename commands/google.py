'''
Created on May 24, 2014

@author: Nicholas
'''

import base_command
import urllib
import random

class CommandGoogle(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"google": "Format: google[ 'search term'] - Creates a google link to a search term"}
        self.bot = bot

    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        #list of query string keys and values, URL encoded
        query_string = urllib.urlencode({'q': arg.lower()})

        if mention_name:
            self.bot.sendMessage(jid_to,
                                 'Ever heard of the Internet? <b><a href="http://www.lmgtfy.com?%s">%s</a></b>' % (query_string, arg), 'html', mention_name)
        else:
            self.bot.sendMessage(jid_to,
                                 'Here you are, sir: <b><a href="http://www.google.com/search?%s">%s</a></b>' % (query_string, arg), 'html', mention_name)

