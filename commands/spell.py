'''
Created on Aug 15, 2013

@author: Andreas
'''

import base_command
import re
import enchant

class CommandSpell(base_command.BaseCommand):

    def __init__(self, bot):
        self.commands = {"spellcheck": "Format: spellcheck x - checks spelling of x"}
        self.bot = bot
        self.dict = enchant.Dict("en_US")
        self.bot.add_event_handler("groupchat_message", self.checkSpelling)
    
    def execute(self, jid_to, from_tuple, mention_name, command, arg):
        suggestions = self.dict.suggest(arg)
        if suggestions:
            self.bot.sendMessage(jid_to, ", ".join(suggestions), 'html', mention_name)
        else:
            self.bot.sendMessage(jid_to, arg+" probably doesn't exist or is a term @neo is too smart to need to have in his (its?) vocabulary", 'text', mention_name)
        
    def checkSpelling(self, word):
        pass