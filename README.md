# README #

## How do I get set up? ##

* Install Python
* Install dependencies
* Create a config file 

## Dependencies ##

### General ###

* Python HipChat - https://github.com/tagged/python-hipchat
* WatchDog - https://pypi.python.org/pypi/watchdog
* SleekXMPP - https://github.com/fritzy/SleekXMPP
* dnspython - http://www.dnspython.org/kits/1.11.1/
* pyasn1 - http://sourceforge.net/projects/pyasn1/
* pyasn1_modules - https://pypi.python.org/pypi/pyasn1-modules/

### Scrambler ###

* WandPy - https://github.com/dahlia/wand/tree/0.3.5
* ImageMagick for Windows - http://docs.wand-py.org/en/latest/guide/install.html#install-imagemagick-on-windows

### Spell ###

* enchant - https://pythonhosted.org/pyenchant/download.html

### Screencast ###

* lxml - http://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml (Don't mistakenly use the amd download if you don't need it)

### Jenkins ###

* jenkinsapi - https://pypi.python.org/pypi/jenkinsapi

## Config File ##

Make a copy of config.cfg.sample named config.cfg and fill in the appropriate information. 

The value for the *room_jid* field of the hipchat section of the config can be found by visiting [here](https://api.hipchat.com/v1/rooms/list?format=json&auth_token=)

You must append the Admin auth token to the above request. Find it by visiting https://elyk.hipchat.com/admin/api

jid is the XMPP jabber ID of neo. You can find this information in HipChat.

room_jid is the room's Jabber ID. Taken from the HipChat website properties of the room.

room_api_id is the ID used for the REST API. This is used for sending messages, so that we can have HTML formatting.

muc_nick is the nickname that the user shows up as. This has to match the name in HipChat's web area for the user you are connecting to.

api_v1_token is the REST API token