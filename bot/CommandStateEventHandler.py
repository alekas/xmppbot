'''
Created on Mar 5, 2014

@author: Andreas
'''

from watchdog.events import FileSystemEventHandler
import logging

class CommandStateEventHandler(FileSystemEventHandler):
    

    def __init__(self, bot):
        self.bot = bot
        
    
    def on_moved(self, event):
        super(CommandStateEventHandler, self).on_moved(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info("Moved %s: from %s to %s", what, event.src_path, event.dest_path)
        self.bot.importCommands();

    def on_created(self, event):
        super(CommandStateEventHandler, self).on_created(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info("Created %s: %s", what, event.src_path)
        self.bot.importCommands();

    def on_deleted(self, event):
        super(CommandStateEventHandler, self).on_deleted(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info("Deleted %s: %s", what, event.src_path)
        self.bot.importCommands();

    def on_modified(self, event):
        super(CommandStateEventHandler, self).on_modified(event)

        what = 'directory' if event.is_directory else 'file'
        logging.info("Modified %s: %s", what, event.src_path)    
        self.bot.importCommands();