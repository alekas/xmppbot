'''
Created on Aug 6, 2013

@author: Andreas
'''

import logging
import inspect
import importlib
from commands.base_command import BaseCommand
from commands.base_listener import BaseListener
import imp
import os
import re
import threading
import time 
import sys
import ConfigParser

import hipchat

from watchdog.observers import Observer
from CommandStateEventHandler import CommandStateEventHandler


from sleekxmpp import ClientXMPP
from sleekxmpp.exceptions import IqError, IqTimeout

import smtplib
from email.mime.text import MIMEText

MODULE_EXTENSIONS = ('.py', '.pyc', '.pyo')

from HTMLParser import HTMLParser
import htmlentitydefs


class MLStripper(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.result = [ ]

    def handle_data(self, d):
        self.result.append(d)

    def handle_charref(self, number):
        codepoint = int(number[1:], 16) if number[0] in (u'x', u'X') else int(number)
        self.result.append(unichr(codepoint))

    def handle_entityref(self, name):
        codepoint = htmlentitydefs.name2codepoint[name]
        self.result.append(unichr(codepoint))

    def get_text(self):
        return u''.join(self.result)
    
class Bot(ClientXMPP):
    command_prefix = "."
    commands = {}
    listeners = []
    command_line = False
    raw_message_listener = {}
    rooms = []
    
    def __init__(self, config):
        ClientXMPP.__init__(self, config.get('hipchat', 'jid'), config.get('hipchat', 'password'))
        self.config = config
        self.importCommands()
        
        self.hipster = hipchat.HipChat(token=config.get('hipchat', 'api_v1_token'))
        
        rooms = config.get('hipchat', 'rooms')
        rooms = rooms.split(',')
        for room in rooms:
            room_parts = room.split(':')
            self.rooms.append(room_parts)
        
        print self.rooms
        #self.room_jid = config.get('hipchat', 'room_jid')
        self.nick = config.get('hipchat', 'muc_nick')
        
        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("groupchat_message", self.receiveMessage)
        
        
         #({'room_id': 435417, 'from':'HAL9000', 'message':'<b>HTML</b> <i>test</i>'})
        
    def session_start(self, event):
        self.send_presence()
        self.get_roster()
        for room in self.rooms:
            print("Joining chat: %s" % room[0])
            self.plugin['xep_0045'].joinMUC(room[0], self.nick, wait=True)
            print("Joined %s" % room[0])
        
    def getCommands(self):
        return self.commands
    
    def getListeners(self):
        return self.listeners
    
    def getCommandPrefix(self):
        return self.command_prefix
    
    def importCommands(self):
        modules = self.package_contents("commands")
        print(sys.getrefcount(self.commands))
        self.commands.clear()
        self.listeners[:] = []
        print self.commands
        for module_name in modules:
            if module_name=="__init__" or module_name=="base_command" or module_name=="state_machine_command" or module_name=="base_listener":
                continue
            print "importing "+module_name
            module = importlib.import_module("commands."+module_name)
            for name, obj in inspect.getmembers(module, predicate=inspect.isclass):
                if (name.find("Command") != -1 or name.find("Listener") != -1) and inspect.isclass(obj):
                    class_ = getattr(module, name)
                    instance = class_(self)
                    if isinstance(instance, BaseCommand):
                        execute_method = getattr(class_(self), "execute")
                        commands = getattr(class_(self), "getCommands")()
                        for command in commands:
                            self.commands[command] = [execute_method, commands[command]]
                    elif isinstance(instance, BaseListener):
                        execute_method = getattr(class_(self), "receiveMessage")
                        description = getattr(class_(self), "getDescription")()
                        self.listeners.append([execute_method, description])
                    
    def package_contents(self, package_name):
        file, pathname, description = imp.find_module(package_name)
        if file:
            raise ImportError('Not a package: %r', package_name)
        # Use a set because some may be both source and compiled.
        return set([os.path.splitext(module)[0]
            for module in os.listdir(pathname)
            if module.endswith(MODULE_EXTENSIONS)])
            
    def receiveMessage(self, stanza):
        
        mention_name = ''
        redirection = ''
        msg = self.strip_html(stanza['body'])
        if(self.command_line):
            to_name = self.nick
            nick = self.nick
        else:
            to_name = stanza['from'].bare
            nick = stanza['mucnick']
            
        from_name = (to_name, nick, '@'+''.join(nick.split(' ')))
        processed_command = False
        if stanza['mucnick'] != self.nick:
            #print user_name, msg
            self.invokeListeners(to_name, from_name, msg)
            if msg[0]==self.command_prefix:
                matches = re.match("^(.*?)([|>](.*?))?$", msg)
                
                if matches:
                    message = matches.groups()[0]
                    
                    redirection = matches.groups()[1]
                    if redirection!=None:
                        if redirection[0]=='>':
                            mention_name = matches.groups()[2].strip()
                        elif redirection[0]=='|':
                            to_name = matches.groups()[2].strip()
                            
                    print message
                    if message.find(' ')==-1:
                        # Single-word command
                        print "Single-word command from "+from_name[1]+": "+message
                        processed_command = self.handleCommand(to_name, from_name, mention_name, message[1:], "")
                    else:
                        #Multi-word command
                        command = message[1:message.find(" ")]
                        argument = message[message.find(command)+len(command)+1:].strip()
                        print "Command from "+from_name[1]+": "+command+", arg: "+argument
                        processed_command = self.handleCommand(to_name, from_name, mention_name, command, argument);
                 
    def handleCommand(self, sn_to, from_tuple, mention_name, command, arg):
        found_command = False
        for k in self.commands:
            if k==command:
                print("Calling %s" % self.commands[k][0])
                self.commands[k][0](sn_to, from_tuple, mention_name, command, arg)
                found_command=True
        return found_command
    
    def invokeListeners(self, to, from_tuple, message):
        for l in self.listeners:
            l[0](to, from_tuple, message)
    
    def strip_html(self, html):
        s = MLStripper()
        s.feed(html)
        return s.get_text()
    
    def sendMessage(self, jid, message, style='html', mention='', color=''):
        if mention!='':
            print (mention)
            if self.validateEmail(mention):
                self.sendEmail(mention, "Message from HipChat", message)
                return
            
            message = '<b>'+mention+"</b>: "+" "+message
            
        if(self.command_line):
            print jid +": "+message
        else:
            '''if jid!=self.config.get('hipchat', 'room_jid'):
                print jid +": "+message
                self.send_message(mto=jid,
                              mbody=self.strip_html(message),
                              mhtml=self.strip_html(message))
            else: '''
            for room in self.rooms:
                if room[0]==jid:
                    self.hipster.message_room(room[1], self.config.get('hipchat', 'muc_nick'), message, style, color, True)
    
    def sendEmail(self, to, subject, message):
        msg = MIMEText(message, "html")
        
        frm = self.config.get('email', 'email')
        
        msg["From"] = frm
        msg["To"] = to
        msg["Subject"] = subject
        s = smtplib.SMTP(self.config.get('email', 'server'), self.config.get('email', 'port'))
        s.ehlo()
        s.starttls()
        s.login(self.config.get('email', 'email'), self.config.get('email', 'password'))
        print "sending mail to "+to+", from: "+frm
        s.sendmail(frm, to, msg.as_string())
    
    # From: http://code.activestate.com/recipes/65215-e-mail-address-validation/
    def validateEmail(self, email):
        if len(email) > 7:
            if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
                return 1
        return 0
    
    def runOnCommandLine(self):
        self.command_line = True
        self.importCommands()
        while True:            
            msg = raw_input("Enter Something: ")
            self.receiveMessage("CmdLine", [[msg]], 0)
            
    def monitorCommands(self):
        path = os.getcwd()+'\..\commands'
        print(path)
        event_handler = CommandStateEventHandler(self)
        observer = Observer()
        observer.schedule(event_handler, path, recursive=False)
        observer.start()
        while True:
            time.sleep(1)
       
if __name__ == '__main__':
    config = ConfigParser.ConfigParser()
    config.readfp(open('../config.cfg'))
    
    xmpp = Bot(config)
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)-8s %(message)s')
    xmpp.register_plugin('xep_0045')
    xmpp.register_plugin('xep_0199') # XMPP Ping
    
    t = threading.Thread(target=xmpp.monitorCommands)
    t.start()
    
    xmpp.whitespace_keepalive = True
    xmpp.whitespace_keepalive_interval = 60
    
    
    if xmpp.connect():
        print("Connected")
        xmpp.process(block=True)
        print("Done")
    else:
        print ("Cannot connect")
    #bot = Bot("commandline", "none")
    #bot.runOnCommandLine()
    
